open Bos
open Rresult

let home = match OS.Dir.user () with
  | Error _ -> exit 1
  | Ok h -> h
let store = Fpath.add_seg home ".password-store"

