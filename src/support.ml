open Bos
open Astring
open Rresult

let ( >>? ) o f = match o with
  | None -> Ok None
  | Some v -> f v

let rec entries_of_contents acc = function
  | [] -> Ok acc
  | path :: paths ->
    let acc =
      if Fpath.has_ext "gpg" path then
        Fpath.rem_ext path :: acc
      else
        match OS.Dir.exists path with
        | Error err ->
          Format.eprintf "Error: %a\n%!" R.pp_msg err;
          acc
        | Ok false ->
          (* Not a directory and not a .gpg *)
          acc
        | Ok true ->
          let subpath_entries =
            List.map
              Fpath.(fun p -> path // p)
              begin
                R.ignore_error
                  ~use:(fun _ -> [] (* TODO: log *))
                  (entries_of_dir path)
              end
          in
          match subpath_entries with
          | [] ->
            (* No entries in the directory, we ignore *)
            acc
          | _ :: _ ->
            (* Note: the whole list is sorted later, rev append for the stack *)
            Fpath.to_dir_path path :: List.rev_append subpath_entries acc
    in
    entries_of_contents acc paths

and entries_of_dir dir =
  OS.Dir.contents ~rel:true dir >>=
  entries_of_contents []

let dmenu entries =
  let entries_block = String.concat ~sep:"\n" (List.map Fpath.to_string entries) in
  OS.Cmd.(in_string entries_block |> run_io (Cmd.v "dmenu") |> to_string)

let drive action =
  let rec step dirstack =
    OS.Dir.current () >>=
    entries_of_dir >>| List.sort Fpath.compare >>=
    dmenu >>| Fpath.v >>= fun entry ->
    if Fpath.is_dir_path entry then
      R.join (OS.Dir.with_current entry step Fpath.(dirstack // entry))
    else
      action dirstack entry
  in
  R.join (OS.Dir.with_current Conf.store step (Fpath.v "./"))
