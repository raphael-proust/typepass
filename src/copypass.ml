open Bos
open Astring
open Rresult

open Support

let copy_pass entry =
  let entry_string = Fpath.to_string entry in
  (* TODO: check output is of expected form *)
  OS.Cmd.(in_null |> run_io Cmd.(v "pass" % "show" % "-c" % entry_string) |> to_null)

let action dirstack entry =
  copy_pass Fpath.(normalize (dirstack // entry))

let () =
  R.ignore_error
    ~use:(fun err -> Format.eprintf "Error: %a\n%!" R.pp_msg err; exit 1)
    (drive action);
  exit 0
