open Bos
open Astring
open Rresult

open Support

(* NOTE: this assumes mouse-follow-focus. *)
let find_current_win_id () =
  OS.Cmd.(run_out Cmd.(v "xdotool" % "getwindowfocus") |> to_string)

let get_pass entry =
  OS.Cmd.(in_null |> run_io Cmd.(v "pass" % "show" % Fpath.to_string entry) |> to_lines) >>= function
  | [] -> R.error_msg "Empty entry, nothing to type"
  | hd :: _ -> R.ok hd

let keypresses win_id text =
  let input =
    let open Format in
    let _: string = flush_str_formatter () in
    fprintf str_formatter "type --clearmodifiers --window %s -- '%s'\n"
      win_id
      text;
    flush_str_formatter ()
  in
  OS.Cmd.(in_string input |> run_in Cmd.(v "xdotool" % "-"))

let action win_id dirstack entry =
  get_pass Fpath.(normalize (dirstack // entry)) >>= fun pass ->
  keypresses win_id pass

let () =
  R.ignore_error
    ~use:(fun err -> Format.eprintf "Error: %a\n%!" R.pp_msg err; exit 1)
    (
      find_current_win_id () >>= fun win_id ->
      drive (action win_id)
    );
  exit 0
