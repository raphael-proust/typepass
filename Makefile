
all: typepass.native copypass.native

typepass.native: src/conf.ml src/support.ml
	mkdir -p _build/
	ocamlfind ocamlopt -package bos,astring,rresult -linkpkg -I src/ src/conf.ml src/support.ml src/typepass.ml -o _build/typepass.native

copypass.native: src/conf.ml src/support.ml
	mkdir -p _build/
	ocamlfind ocamlopt -package bos,astring,rresult -linkpkg -I src/ src/conf.ml src/support.ml src/copypass.ml -o _build/copypass.native

src/conf.ml:
	cp src/conf.def.ml src/conf.ml

clean:
	rm -rf src/*.cm* src/*.o _build/
